# Search and replace

If I have renamed an entity to another name, this is the simplest way to rename my scripts as well:

```sh
find -iname '*.yaml' -not -path './custom_components/*' | xargs sed -i 's#.entertainment#.plug_entertainment#g' --debug
```
