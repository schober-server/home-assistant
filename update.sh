#!/bin/sh

script_dir="$(dirname "$(realpath "$0")")"
cd "$script_dir" || exit
echo "Running in $script_dir 💪"

docker compose build --pull
"$script_dir/restart.sh"
