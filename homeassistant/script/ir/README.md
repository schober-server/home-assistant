# Infrared setup

I am currently using a TS1201 with this Quirk:
`TS1201-ir-blaster.ZosungIRBlaster_ZS06`

You can find mine [here](https://gitlab.com/schober-server/home-assistant/-/blob/7a0e4a7ea29f70d9f3459748f31bde5472e3c6fb/homeassistant/custom_zha_quirks/TS1201-ir-blaster.py)

## Creating a new automation

First of, you need the payload to send.
To get the payload open the device properties in HA and press `Manage Zigbee Device`:

![Clicking "Manage Zigbee Device"](https://gitlab.com/schober-server/documentation/images/-/raw/e09d218771a9acb2933c01727e46a7719e298ee4/home-assistant/ir-01-manage-device.png)


### Enabling learning mode 🧠

Put the device into learning mode: `Cluster[0xe004] -> Commands -> IRLearn[0x0001]` and press `Issue Zigbee command`:

!["Enabling learning mode"](https://gitlab.com/schober-server/documentation/images/-/raw/e09d218771a9acb2933c01727e46a7719e298ee4/home-assistant/ir-02-learn.png)


### Get payload 💁

Now point your old remote at the TS1201 and press the commands you want to save.
Make sure that nobody else sends IR commands at that time.

Then navigate to `Cluster[0x004] -> Attributes -> last_learned_ir_code[0x0000]` and press `Read attribute` to see the payload:

!["Popup with the payload"](https://gitlab.com/schober-server/documentation/images/-/raw/e09d218771a9acb2933c01727e46a7719e298ee4/home-assistant/ir-03-show-payload.png)

Save this payload to clipboard or to a text file as you need it for your automation.

### Test payload (optional) 🧪

If you don't want to create an automation right away, you can just test it in this `Manage Zigbee Device`-Popup

Go back to: `Cluster[0xe004] -> Commands -> IRSend[0x0002]` enter the payload under `code` and press `Issue Zigbee command`:

!["Enabling learning mode"](https://gitlab.com/schober-server/documentation/images/-/raw/e09d218771a9acb2933c01727e46a7719e298ee4/home-assistant/ir-04-test-payload.png)

Now your device (if it can see the TS1201) should receive your command 🎉
