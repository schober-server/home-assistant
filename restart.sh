#!/bin/sh

script_dir="$(dirname "$(realpath "$0")")"
cd "$script_dir" || exit

docker compose down
docker compose up -d
