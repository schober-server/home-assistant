import json

with open('homeassistant/.storage/core.restore_state_bak', 'r', encoding='utf-8') as f:
    json_data = json.load(f)
    ha_data = json_data['data']

    for value in ha_data:
        try:
            uom = value['state']['attributes']['unit_of_measurement']

            if uom == 'kWh':
                #print(f"Setting value of {value['state']['entity_id']} to 0")
                value['state']['state'] = "0"

        except:
            continue

    #print("\n\n\n\n")
    print(json_data)
